﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring
{
    internal class ParnerSetLimit : DefaultFactory, IFactory
    {
        Guid IFactory.GetGuid()
        {
            return It.IsAny<Guid>();
        }

        Partner IFactory.GetPartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 1,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
        }

        SetPartnerPromoCodeLimitRequest IFactory.GetRequest()
        {
            return new SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Now.AddDays(30), Limit = 1 };
        }

        void IFactory.Check(IActionResult result)
        {
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}
