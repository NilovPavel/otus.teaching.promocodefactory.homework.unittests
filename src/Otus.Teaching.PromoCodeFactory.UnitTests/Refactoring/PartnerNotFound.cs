﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring
{
    internal class PartnerNotFound : DefaultFactory, IFactory
    {
        void IFactory.Check(IActionResult result)
        {
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        Partner IFactory.GetPartner()
        {
            return null;
        }
    }
}