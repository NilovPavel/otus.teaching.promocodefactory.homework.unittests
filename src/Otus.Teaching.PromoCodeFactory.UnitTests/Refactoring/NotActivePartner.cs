﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring
{
    internal class NotActivePartner : DefaultFactory, IFactory
    {
        Partner IFactory.GetPartner()
        {
            return new Partner { Id = Guid.NewGuid(), IsActive = false };
        }

        void IFactory.Check(IActionResult result)
        {
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value.Should().Be("Данный партнер не активен");
        }
    }
}
