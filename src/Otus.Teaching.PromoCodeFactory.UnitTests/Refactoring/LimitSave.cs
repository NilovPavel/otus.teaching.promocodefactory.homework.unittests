﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring
{
    public class LimitSave : DefaultFactory, IFactory
    {
        SetPartnerPromoCodeLimitRequest IFactory.GetRequest()
        {
            return new SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Now.AddDays(30), Limit = 1 };
        }
    }
}
