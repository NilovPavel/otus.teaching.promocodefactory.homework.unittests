﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring
{
    /// <summary>
    /// Класс фабрики по производству данных для тестирования
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// Получает объект запроса на установку лимита
        /// </summary>
        /// <returns>Возвращает объект типа SetPartnerPromoCodeLimitRequest</returns>
        public SetPartnerPromoCodeLimitRequest GetRequest();

        /// <summary>
        /// Получает объект партнера
        /// </summary>
        /// <returns>Возвращает объект Partner</returns>
        public Partner GetPartner();

        /// <summary>
        /// Получает GUID партнера
        /// </summary>
        /// <returns>Возвразает объект типа Guid</returns>
        public Guid GetGuid();

        /// <summary>
        /// Производит проверку результата запроса
        /// </summary>
        /// <param name="result">Ответ полученный в методе установки лимита</param>
        public void Check(IActionResult result);
    }
}
