﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring
{
    internal class Limit0 : DefaultFactory, IFactory
    {
        Partner IFactory.GetPartner()
        {
            return this.GetPartner();
        }

        SetPartnerPromoCodeLimitRequest IFactory.GetRequest()
        {
            return new SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Now.AddDays(30), Limit = 0 };
        }

        void IFactory.Check(IActionResult result)
        {
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            (result as BadRequestObjectResult).Value.Should().Be("Лимит должен быть больше 0");
        }
    }
}
