﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Refactoring;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = new PartnersController(this._partnersRepositoryMock.Object);
        }

        //Если партнер не найден, то также нужно выдать ошибку 404;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PatnerIsNotFound_ReturnNotFound()
        {
            IFactory factory = new PartnerNotFound();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(factory.GetGuid())).ReturnsAsync(factory.GetPartner());

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetGuid(), factory.GetRequest());

            // Assert
            factory.Check(result);
        }

        [Fact]
        //Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        public async void SetPartnerPromoCodeLimitAsyncTests_PatnerIsNotActive_ReturnNotFound()
        {
            IFactory factory = new NotActivePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(factory.GetPartner().Id)).ReturnsAsync(factory.GetPartner());

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(
                factory.GetPartner().Id, factory.GetRequest());

            // Assert
            factory.Check(result);
        }

        [Fact]
        //Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerSetLimit_NumberIssuedPromoCodesIs0()
        {
            IFactory factory = new ParnerSetLimit();
            Partner partner = factory.GetPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, factory.GetRequest());

            // Assert
            factory.Check(result);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        //если лимит закончился, то количество не обнуляется
        public async void SetPartnerPromoCodeLimitAsyncTests_ThereIsALimitFinished_NumberIssuedPromoCodesIsNot0()
        {
            IFactory factory = new ParnerSetLimit();
            Partner partner = factory.GetPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).CancelDate = DateTime.Now.AddDays(-1);        /*Закончить лимит*/
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, factory.GetRequest());           /*Задать лимит пользователю*/

            // Assert
            factory.Check(result);

            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        [Fact]
        //При установке лимита нужно отключить предыдущий лимит;
        public async void SetPartnerPromoCodeLimitAsyncTests_SetLimit_DisablePreviousLimit()
        {
            IFactory factory = new ParnerSetLimit();
            Partner partner = factory.GetPartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            PartnerPromoCodeLimit lastActiveLimit = partner.PartnerLimits.LastOrDefault(x => !x.CancelDate.HasValue);       /*Получение последнего неотключенного лимита*/
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, factory.GetRequest());           /*Задать лимит пользователю*/

            // Assert
            factory.Check(result);

            lastActiveLimit.CancelDate.HasValue.Should().BeTrue();
        }

        [Fact]
        //Лимит должен быть больше 0;
        public async void SetPartnerPromoCodeLimitAsyncTests_SetLimit0_ReturnBadResult()
        {
            IFactory factory = new Limit0();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(factory.GetPartner().Id)).ReturnsAsync(factory.GetPartner());

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetPartner().Id, factory.GetRequest());           /*Задать лимит пользователю*/
            
            // Assert
            factory.Check(result);
        }

        [Fact]
        //Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        public async void SetPartnerPromoCodeLimitAsyncTests_SendLimit_LimitExists()
        {
            IFactory factory = new LimitSave();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(factory.GetPartner().Id)).ReturnsAsync(factory.GetPartner());

            // Act
            IActionResult result = await _partnersController.SetPartnerPromoCodeLimitAsync(factory.GetPartner().Id, factory.GetRequest());           /*Задать лимит пользователю*/

            // Assert
            
            factory.Check(result);

            Partner partnerInDB = await _partnersRepositoryMock.Object.GetByIdAsync(factory.GetPartner().Id);
            partnerInDB.PartnerLimits.Should().NotBeEmpty();
        }
    }
}