﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.FabricMethod
{
    internal interface IFabricMethod<T,S> 
        where T : BaseEntity
        where S : ControllerBase
    {
        public Mock<IRepository<T>> GetpartnersRepositoryMock();
        public S GetControllerBase();
    }
}
