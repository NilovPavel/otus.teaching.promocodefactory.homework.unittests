﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.FabricMethod
{
    public class FabricMethodPatners<T, S> 
        : IFabricMethod<Partner, PartnersController>
    {
        private IFixture fixture;

        public FabricMethodPatners()
        {
            this.Initialization();
        }

        private void Initialization()
        {
            this.fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        public PartnersController GetControllerBase()
        {
            return fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Mock<IRepository<Partner>> GetpartnersRepositoryMock()
        {
            return fixture.Freeze<Mock<IRepository<Partner>>>();
        }
    }
}
